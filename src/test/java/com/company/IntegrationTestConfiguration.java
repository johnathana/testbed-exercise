package com.company;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan
@Import(AppConfig.class)
@ImportResource({"classpath:testContext.xml"})
public class IntegrationTestConfiguration {

}
