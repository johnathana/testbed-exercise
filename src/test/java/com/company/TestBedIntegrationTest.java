package com.company;

import com.company.IntegrationTestConfiguration;
import com.company.repository.EpantRepository;
import com.company.service.SchedulerService;
import com.company.task.EpantGetNewsJob;
import com.company.utils.HttpUtils;
import org.eclipse.jetty.webapp.WebAppContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {IntegrationTestConfiguration.class, WebAppContext.class})
@WebAppConfiguration
public class TestBedIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private EpantRepository epantRepository;

    @Autowired
    private SchedulerService schedulerService;


    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    public void testIndexRedirect() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/index.zul"));
    }

    @Test
    public void testJobSchedulerAndGetNewsEndpoint() throws Exception {

        executeScheduler();

        String epantJson = HttpUtils.getUrlContent(EpantGetNewsJob.EPANT_URL);

        mockMvc.perform(get("/GetMyNews"))
                .andExpect(status().isOk())
                .andExpect((content().contentType("application/json;charset=UTF-8")))
                .andExpect((content().json(epantJson)));
    }

    private void executeScheduler() throws InterruptedException {
        schedulerService.executeOnDemand();
        // wait for the scheduler to do his job
        Thread.sleep(5000);
    }
}
