
CREATE TABLE EPANT_DATA (
  ID                            INT PRIMARY KEY,
  FileName                      VARCHAR2(255 CHAR),
  FilePath                      VARCHAR2(255 CHAR),
  IsFile                        BOOLEAN
);

CREATE TABLE DATA (
  ID                            INT PRIMARY KEY AUTO_INCREMENT,
  EPANT_DATA_ID                 INT,
  Key                           VARCHAR2(255 CHAR),
  Value                         VARCHAR2(2048 CHAR),
  FOREIGN KEY (EPANT_DATA_ID) REFERENCES EPANT_DATA(ID)
);

CREATE SEQUENCE HIBERNATE_SEQUENCE;
