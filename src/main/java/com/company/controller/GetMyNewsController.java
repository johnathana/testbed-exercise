package com.company.controller;

import com.company.pojo.generated.EpantDatum;
import com.company.repository.EpantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetMyNewsController {

    @Autowired
    private EpantRepository epantRepository;

    @RequestMapping(path="/GetMyNews", method= RequestMethod.GET)
    public List<EpantDatum> getNews() {
        return epantRepository.findAll();
    }
}
