package com.company.service;

import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

@Service("schedulerService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class QuartzSchedulerService implements SchedulerService {

    private final static Logger logger = Logger.getLogger(QuartzSchedulerService.class);

    private Scheduler quartzScheduler;

    private JobDetail jobDetail;


    public QuartzSchedulerService(@Autowired SchedulerFactoryBean schedulerFactoryBean,
                                  @Autowired JobDetail jobDetail) throws SchedulerException {
        this.jobDetail = jobDetail;
        schedulerFactoryBean.setJobDetails(jobDetail);
        quartzScheduler = schedulerFactoryBean.getScheduler();
        quartzScheduler.addJob(jobDetail, true);
        quartzScheduler.start();
    }

    @Override
    public void executeOnDemand() {
        try {
            quartzScheduler.triggerJob(jobDetail.getKey());
        } catch (SchedulerException e) {
            logger.error("QuartzSchedulerService executor on demand failed", e);
        }
    }
}
