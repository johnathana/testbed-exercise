package com.company.pojo.generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Data",
        "FileName",
        "FilePath",
        "ID",
        "IsFile"
})
@Entity
@Table(name = "EPANT_DATA")
public class EpantDatum {

    @JsonProperty("ID")
    @Id
    private Integer id;

    @JsonProperty("Data")
    @OneToMany(targetEntity = Datum.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true)
    @JoinColumn(name = "epant_data_id")
    private List<Datum> data = new ArrayList<>();

    @JsonProperty("FileName")
    @Column(name = "FileName")
    private String fileName;

    @JsonProperty("FilePath")
    @Column(name = "FilePath")
    private String filePath;

    @JsonProperty("IsFile")
    @Column(name = "IsFile")
    private Boolean isFile;

    @JsonProperty("Data")
    public List<Datum> getData() {
        return data;
    }

    @JsonProperty("Data")
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @JsonProperty("FileName")
    public String getFileName() {
        return fileName;
    }

    @JsonProperty("FileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @JsonProperty("FilePath")
    public String getFilePath() {
        return filePath;
    }

    @JsonProperty("FilePath")
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @JsonProperty("ID")
    public Integer getID() {
        return id;
    }

    @JsonProperty("ID")
    public void setID(Integer iD) {
        this.id = iD;
    }

    @JsonProperty("IsFile")
    public Boolean getIsFile() {
        return isFile;
    }

    @JsonProperty("IsFile")
    public void setIsFile(Boolean isFile) {
        this.isFile = isFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EpantDatum datum = (EpantDatum) o;
        return Objects.equals(id, datum.id) &&
                Objects.equals(data, datum.data) &&
                Objects.equals(fileName, datum.fileName) &&
                Objects.equals(filePath, datum.filePath) &&
                Objects.equals(isFile, datum.isFile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, data, fileName, filePath, isFile);
    }
}
