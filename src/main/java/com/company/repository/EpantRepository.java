package com.company.repository;

import com.company.pojo.generated.EpantDatum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("epantRepository")
public interface EpantRepository extends JpaRepository<EpantDatum, Long> {

}
