package com.company;

import com.company.repository.EpantRepository;
import com.company.task.EpantGetNewsJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJpaRepositories("com.company.repository")
public class AppConfig {

    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);

    private static final String JOB_NAME = "epant_get_news";

    @Value("classpath:db/create_schema.sql")
    private Resource schemaScript;


    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(EpantRepository epantRepository) {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setJobFactory(new SpringBeanJobFactory());
        Map<String, Object> contextAsMap = new HashMap<>();
        contextAsMap.put("epantRepository", epantRepository);
        schedulerFactoryBean.setSchedulerContextAsMap(contextAsMap);
        return schedulerFactoryBean;
    }

    @Bean
    public JobDetail jobDetail() {
        return JobBuilder.newJob().ofType(EpantGetNewsJob.class)
                .storeDurably()
                .withIdentity(JOB_NAME)
                .withDescription("Fetch epant.gr news")
                .build();
    }

    @Bean
    public DataSource dataSource() {
        logger.info("Database Mode: H2");
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.h2.Driver");
        driverManagerDataSource.setUrl("jdbc:h2:mem:testbeddb;DB_CLOSE_DELAY=-1");
        driverManagerDataSource.setUsername("user");
        driverManagerDataSource.setPassword("");
        return driverManagerDataSource;
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
        logger.info("Populate H2 schema");
        DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource);
        initializer.setDatabasePopulator(databasePopulator());
        return initializer;
    }

    private DatabasePopulator databasePopulator() {
        final ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.addScript(schemaScript);
        return populator;
    }
}
