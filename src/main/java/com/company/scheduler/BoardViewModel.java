package com.company.scheduler;

import com.company.service.SchedulerService;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import static org.zkoss.zk.ui.util.Clients.showNotification;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class BoardViewModel {

    @WireVariable
    private SchedulerService schedulerService;

    @Command
    public void startGetNewsJob() {
        //launch job epant_get_news on demand and return
        schedulerService.executeOnDemand();

        showNotification("I am the server, you just poked me!", true);
    }
}
