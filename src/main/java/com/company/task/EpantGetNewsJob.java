package com.company.task;

import com.company.pojo.generated.EpantDatum;
import com.company.repository.EpantRepository;
import com.company.utils.HttpUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;


public class EpantGetNewsJob extends QuartzJobBean {

    public static final String EPANT_URL = "https://www.epant.gr/openData.svc/GetNews";

    private final static Logger logger = Logger.getLogger(EpantGetNewsJob.class);

    private EpantRepository repository;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("EpantGetNewsJob triggered");

        SchedulerContext schedulerContext;
        try {
            schedulerContext = jobExecutionContext.getScheduler().getContext();
            repository = (EpantRepository)schedulerContext.get("epantRepository");

            getNewsAndPersist();

        } catch (SchedulerException e) {
            logger.error("Repository cound not be injected", e);
        }
    }

    private void getNewsAndPersist() {
        try {
            String json = HttpUtils.getUrlContent(EPANT_URL);
            EpantDatum[] epantData = objectMapper.readValue(json, EpantDatum[].class);

            for (EpantDatum e : epantData) {
                repository.save(e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
